<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller
{
    public function getLead(Request $request, $leadId)
    {
        // Define variables
        $result = [
            'result' => false,
            'data' => []
        ];

        $lead = $this->getAndValidateLead($leadId);

        if ($lead === false) {
            return response()->json($result);
        }

        // Store lead data
        $result = [
            'result' => true,
            'data' => [
                'amount_subscription_sold' => $lead->amount_subscription_sold,
                'operator_sold' => $lead->operator_sold,
                'price' => $lead->price,
                'comment' => $lead->comment
            ]
        ];

        // Return result
        return response()->json($result);
    }

    public function saveLead(Request $request, $leadId)
    {
        $validator = Validator::make($request->all(), [
            'amount_subscription_sold' => 'sometimes|integer|min:1',
            'operator_sold' => 'sometimes|255',
            'price' => 'sometimes|integer|min:1',
            'comment' => 'sometimes|string|max:2000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => false,
                'errors' => $validator->errors()
            ]);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function saveStatus(Request $request, $leadId)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|string|in:new,sold,waiting_customer,waiting_us,sendt,avoided,credit_failed,canceled'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => false,
                'errors' => $validator->errors()
            ]);
        }

        $lead = $this->getAndValidateLead($leadId);

        if ($lead === false) {
            return response()->json($result);
        }

        $lead->status = $request->status;
        $lead->save();

        return response()->json([
            'result' => true
        ]);
    }

    private function getAndValidateLead($leadId) {

        // Find lead
        $lead = Lead::find($leadId);

        if (!$lead) {
            return false;
        }

        // Match lead with users sites
        $siteMatch = auth()->user()->sites()->where('id', $lead->site_id)->get();

        if ($siteMatch->count() === 0) {
            return false;
        }

        return $lead;
    }
}

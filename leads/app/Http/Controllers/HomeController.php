<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $leads = Lead::get();
        return view('home')
            ->with('leads', $leads);
    }

    public function save(Request $req) {


        // print_r($req->input());
        $data = new Lead;
        $data->price = $req->price;
        $data->save();
        return redirect('home');
    }


    public function update(Request $req) {
        Lead::where('id', $req->id)
            ->update([
                'price'=>$req->price,
                'status'=>$req->status,
                'comment'=>$req->comment,
                'service_sold'=>$req->service_sold,
                'amount_subscription_sold'=>$req->amount_subscription_sold,
                'operator_sold'=>$req->operator_sold
            ]);
        // print_r($req->input());

        return redirect('home');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;

    protected $table = 'leads';

    public $timestamp = false;

    protected $fillable = [
        'price',
        'status',
        'comment',
        'service_sold',
        'amount_subscription_sold',
        'operator_sold'
    ];
}

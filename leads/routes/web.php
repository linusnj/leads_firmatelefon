<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/update', [App\Http\Controllers\HomeController::class, 'update']);

Route::get('/sites', [App\Http\Controllers\SitesController::class, 'index'])->name('sites');
Route::post('/create', [App\Http\Controllers\SitesController::class, 'create']);

Route::get('/leads/{leadId}', [App\Http\Controllers\LeadController::class, 'getLead']);
Route::post('/leads/{leadId}', [App\Http\Controllers\LeadController::class, 'saveLead']);
Route::post('/leads/{leadId}/status', [App\Http\Controllers\LeadController::class, 'saveStatus']);

// Route::get('/home', function () {
//     $leads = DB::select('select * from leads');
//     return view('home',['leads'=>$leads]);
// });

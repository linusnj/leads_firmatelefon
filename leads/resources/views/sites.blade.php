@extends('layouts.app')

@section('content')

<body>
    <div>
        <h1 class="text-center">Overiskt over sider for: {{ Auth::user()->name }}</h1>
    </div>
    <div class="d-flex flex-row mx-auto justify-content-center mt-4">
        @foreach ($sites as $key => $site)
        <div class="d-flex p-2">
            <div class="card p-5 card-site">
                <h1>{{ $site->sitename }}</h1>
                <a href="{{ $site->url }}">{{ $site->url }}</a>
            </div>
        </div>
        @endforeach
    </div>
    <div>
        <form action="/create" method="POST">
        @csrf
            <label for="sitename">Sidenavn</label>
            <input type="text" name="sitename">
            <label for="url">Url</label>
            <input type="text" name="url">
            <button type="submit" class="btn btn-primary">Lagre</button>
        </form>
    </div>
</body>

@endsection

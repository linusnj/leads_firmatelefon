@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div>
                        <h3>Leads for firmatelefon.no</h3>
                    </div>
                    <div>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newLead">
                            Ny lead
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {{-- <div class="my-2 d-flex">
                        <input class="form-control" type="text" placeholder="Søk">
                        <button type="submit" class="btn btn-primary">Søk</button>
                    </div> --}}
                    <div>
                        <table class="table table-hover" id="leads">
                            <thead>
                                <tr>
                                    <th scope="col"><h5>ID</h5></th>
                                    <th scope="col"><h5>Dato</h5></th>
                                    <th scope="col"><h5>Bedrift</h5></th>
                                    <th scope="col"><h5>Tlf</h5></th>
                                    <th scope="col"><h5>E-postadresse</h5></th>
                                    <th scope="col"><h5>Antall abonnement / solgt</h5></th>
                                    <th scope="col"><h5>Preferert dekning</h5></th>
                                    <th width="20%" scope="col"><h5>Tillegsinformasjon</h5></th>
                                    <th width="15%" scope="col"><h5><strong>Kommentar</strong></h5></th>
                                    <th width="5%" scope="col"><h5><strong>Kontraktsverdi</strong></h5></th>
                                    <th width="7%" scope="col"><h5><strong>Status</strong></h5></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($leads->sortByDesc('id') as $key => $data)
                                    <tr data-id={{ $data->id }}
                                    @if ($data->status == 'sold') class="table-success"
                                    @elseif ($data->status == 'waiting_customer') class="table-secondary"
                                    @elseif ($data->status == 'waiting_us') class="table-warning"
                                    @elseif ($data->status == 'sendt') class="table-primary"
                                    @elseif ($data->status == 'avoided') class="table-warning"
                                    @elseif ($data->status == 'credit_failed') class="table-danger"
                                    @elseif ($data->status == 'canceled') class="table-danger"
                                    @else class="" @endif>
                                        <td scope="row">{{ $data->id }}</td>
                                        <td scope="row">{{ $data->dato }}</td>
                                        <td scope="row"><a href="https://proff.no/bransjesøk?q={{ $data->companyname }}" target="_blank">{{ $data->companyname }}</a></td>
                                        <td><a href="tel:{{ $data->phone }}">{{$data->phone}}</a></td>
                                        <td><a href="mailto:{{$data->email}}">{{ $data->email }}</a></td>
                                        <td><span class=""> @if ($data->status == 'sold' && isset($data->amount_subscription_sold))
                                            Behøver: {{ $data->subscription_amount }} / <strong>Solgt: {{ $data->amount_subscription_sold }}</strong>
                                        @else {{ $data->subscription_amount}} @endif </span></td>
                                        <td>{{$data->prefered_phone_service}}</td>
                                        <td>{{ $data->other}}</td>
                                        <td><i>{{$data->comment}}</i></td>
                                        <td>
                                            <input type="text" disabled name="price" class="form-control" id="priceinput" data-id="{{ $data->id }}" placeholder="0" value="{{$data->price}}">
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                        </td>
                                        <td>
                                            <select id="status" class="form-select status">
                                                <option value="new" @if ($data->status == 'new') selected="selected" @else @endif>NY!</option>
                                                <option value="sold" @if ($data->status == 'sold') selected="selected" @else @endif>Solgt</option>
                                                <option value="waiting_customer" @if ($data->status == 'waiting_customer') selected="selected" @else @endif>Venter på kunde</option>
                                                <option value="waiting_us" @if ($data->status == 'waiting_us') selected="selected" @else @endif>Venter på oss</option>
                                                <option value="sendt" @if ($data->status == 'sendt') selected="selected" @else @endif>Sendt tilbud</option>
                                                <option value="avoided" @if ($data->status == 'avoided') selected="selected" @else @endif>Unngår</option>
                                                <option value="credit_failed" @if ($data->status == 'credit_failed') selected="selected" @else @endif>Feilet Kreditt</option>
                                                <option value="canceled" @if ($data->status == 'canceled') selected="selected" @else @endif>Nei</option>
                                            </select>
                                        </td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary show-details">

                                                Detaljer
                                                {{-- <i class="fas fa-cog"></i> --}}
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit lead -->
<div class="modal fade" id="editLead" tabindex="-1" role="dialog" aria-labelledby="editLeadLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Rediger info for lead: <strong id="companyname"></strong></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col">
                        <label for="amount_subscription_sold">Antall abonnement solgt</label>
                        <input id="amount_subscription_sold" class="form-control">
                    </div>
                    <div class="form-group col">
                        <label for="operator_sold">Operatør</label>
                        <input id="operator_sold" class="form-control">
                    </div>
                </div>
                <div class="mt-2">
                    <label for="price">Kontraktsverdi i kroner</label>
                    <input id="price" class="form-control">
                </div>
                <div class="mt-4">
                    <label for="comment">Kommentar</label>
                    <textarea rows="6" cols="50" id="comment" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Lukk</button>
                <button type="submit" class="btn btn-primary" id="editLeadComplete">Lagre</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal New lead -->
<div class="modal fade" id="newLead" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Lag en ny lead</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col">
                        <label for="companyname">Bedrift</label>
                        <input name="companyname" class="form-control" placeholder="Bedriftsnavn">
                    </div>
                    <div class="form-group col">
                        <label for="phone">Telefon</label>
                        <input type="phone" name="phone" class="form-control" placeholder="413 14 131">
                    </div>
                </div>
                <div class="mt-2">
                    <label for="price">Kontraktsverdi i kroner</label>
                    <input name="price" class="form-control" value="">
                </div>
                <div class="mt-4">
                    <label for="comment">Kommentar</label>
                    <textarea rows="4" cols="50" name="comment" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Lukk</button>
                <button type="submit" class="btn btn-primary">Lagre</button>
            </div>
        </div>
    </div>
</div>

<style>

.container {
    max-width: 1920px!important;
}

.editRow {
    display: none!important;
    background-color: rgba(129, 255, 146, 0.212)!important;
}


</style>
@endsection

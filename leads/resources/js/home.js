const { default: axios } = require("axios");

$(function () {

    // Show details
    $('table#leads tbody button.show-details').on('click', function(e) {
        var leadId = $(this).parents('tr').attr('data-id');

        axios.get('/leads/' + leadId)
        .then((response) => {
            if (response.data.result === true) {

                var dataFromApi = response.data.data;

                // Update modal data
                $('div.modal#editLead input#amount_subscription_sold').val(dataFromApi.amount_subscription_sold === null ? '' : dataFromApi.amount_subscription_sold);
                $('div.modal#editLead input#operator_sold').val(dataFromApi.operator_sold === null ? '' : dataFromApi.operator_sold);
                $('div.modal#editLead input#price').val(dataFromApi.price === null ? '' : dataFromApi.price);
                $('div.modal#editLead textarea#comment').val(dataFromApi.comment === null ? '' : dataFromApi.comment);

                // Show modal (if not visible)
                var modalEditLead = new bootstrap.Modal(document.getElementById('editLead'));

                if (!$('body').hasClass('modal-open')) {
                    modalEditLead.show();
                }
            }
        }, (error) => {
            console.log(error);
        });
    });

    // Save status
    $('table#leads tbody select.status').on('change', function(e) {

        var _self = this;

        var trRow = $(_self).parents('tr');

        var leadId = trRow.attr('data-id');
        var status = trRow.find('select.status').val();

        axios.post('/leads/' + leadId + '/status', {
            'status': status
        })
        .then((response) => {
            if (response.data.result === true) {
                console.log('ok');

                var statusToClassMap = {
                    'sold': 'table-success',
                    'waiting_customer': 'table-secondary',
                    'waiting_us': 'table-warning',
                    'sendt': 'table-primary',
                    'avoided': 'table-warning',
                    'credit_failed': 'table-danger',
                    'canceled': 'table-danger'
                };

                for (var key in statusToClassMap) {
                    if (!statusToClassMap.hasOwnProperty(key)) {
                        continue;
                    }

                    var currentValue = statusToClassMap[key];

                    if (trRow.hasClass(currentValue)) {
                        trRow.removeClass(currentValue);
                    }
                }

                if (!trRow.hasClass(statusToClassMap[status])) {
                    trRow.addClass(statusToClassMap[status]);
                }
            }
        }, (error) => {
            console.log(error);
        });
    });
});









const getVal = () => {
    //const inputId = document.getElementById('priceinput').getAttribute('data-id');

    const element = document.querySelectorAll("#priceinput");
    let formatter = new Intl.NumberFormat('no-NB', {
        style: 'currency',
        currency: 'NOK',
      });

    for (var i = 0; i < element.length; i++) {
        element[i].value = formatter.format(element[i].value);
    }
    // const value = document.getElementById('priceinput');



    element.value = formatter.format(element.value);

}

window.onload = function() {
    getVal();
  };

